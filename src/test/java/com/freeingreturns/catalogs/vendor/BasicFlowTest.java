package com.freeingreturns.catalogs.vendor;

import static org.junit.Assert.assertEquals;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.junit.Test;

import com.freeingreturns.catalogs.vendor.driver.BasicFlow;
import com.neeve.cli.annotations.Configured;

/**
 * A test case that tests the application flow. 
 */
public class BasicFlowTest extends AbstractTest {

    @Configured(property = "service.vendor.tests.basic.autoStart", defaultValue = "true")
    private boolean autoStart;

    /**
     * 
     */
    private Properties setupEnv() {
        // configure
        Properties env = new Properties();
        env.put("nv.ddl.profiles", "junit");
        env.put("x.apps.vnd-instA.storage.clustering.enabled", "false");
        env.put("x.apps.vnd-instB.storage.clustering.enabled", "false");
        env.put("service.vendor.autoStart", Boolean.toString(autoStart));
        return env;
    }

    @Test
    public void testFlow() throws Throwable {
        int sendCount = 2;
        int sendRate = 1000;
        Properties env = setupEnv();

        // start apps
        startApp(VendorCatalogMain.class, "vnd-processor", "vnd-instA", env);

        // start the send driver
        BasicFlow sender = startApp(BasicFlow.class, "vnd-basicflow", "vnd-utils", env);
        if ( !autoStart )
            sender.doSend(sendCount, sendRate);
        else
            Thread.sleep(5000);
        
        sender.getLatch().await(10000, TimeUnit.MILLISECONDS);
        assertEquals(sender.getLatch().getCount(), 0);
    }
}
