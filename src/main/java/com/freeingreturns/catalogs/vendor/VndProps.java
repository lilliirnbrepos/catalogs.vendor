package com.freeingreturns.catalogs.vendor;
/****************************************************************************
 * FILE: VndProps.java
 * DSCRPT:
 ****************************************************************************/

import com.neeve.aep.AepEngine;
import com.neeve.aep.AepMessageSender;
import com.neeve.cli.annotations.Configured;
import com.neeve.rog.log.RogLog;
import com.neeve.rog.log.RogLogCdcProcessor;
import com.neeve.trace.Tracer;
import com.neeve.trace.Tracer.Level;

public class VndProps {

    protected static final Tracer logger = Tracer.create("service.vendor", Level.DEBUG);

    protected AepMessageSender msgSender;
    protected AepEngine aepEngine;
    protected CDCVndHandler cdchandler;
    protected RogLog log;
    protected RogLogCdcProcessor processor;
    
    
    @Configured(property = "service.vendor.channels.replyChannel")
    protected String rplyChannel;

    @Configured(property = "service.vendor.returnValueOnQueryFail", defaultValue = "false")
    protected boolean returnValueOnQueryFail;

    /**
     * 
     */
    public final boolean getReturnValueOnQueryFail() {
        return returnValueOnQueryFail;
    }

    /**
     * 
     */
    public final void setReturnValueOnQueryFail(boolean returnValueOnQueryFail) {
        this.returnValueOnQueryFail = returnValueOnQueryFail;
    }

    
        
}
