/****************************************************************************
 * FILE: EmployeeCatalog.java
 * DSCRPT:
 ****************************************************************************/

package com.freeingreturns.catalogs.vendor;


import com.freeingreturns.catalogs.vendor.state.Employee;
import com.freeingreturns.catalogs.vendor.state.Repository;
import com.freeingreturns.catalogs.vendor.state.RetailStore;
import com.freeingreturns.roe.vendor.EmployeeDTO;
import com.freeingreturns.roe.vendor.RplyEmployeeAdd;
import com.freeingreturns.roe.vendor.RplyEmployeeQuery;
import com.freeingreturns.roe.vendor.RqstEmployeeAdd;
import com.freeingreturns.roe.vendor.RqstEmployeeQuery;
import com.freeingreturns.utils.FRLogger;
import com.neeve.aep.AepMessageSender;
import com.neeve.aep.annotations.EventHandler;
import com.neeve.cli.annotations.Configured;
import com.neeve.server.app.annotations.AppInjectionPoint;
import com.neeve.trace.Tracer.Level;

public class EmployeeCatalog { //extends VndProps {

    protected AepMessageSender msgSender;

    @Configured(property = "service.vendor.channels.replyChannel")
    protected String rplyChannel;

    @Configured(property = "service.vendor.returnValueOnQueryFail", defaultValue = "false")
    protected boolean returnValueOnQueryFail;

    public final boolean getReturnValueOnQueryFail() {
        return returnValueOnQueryFail;
    }

    public final void setReturnValueOnQueryFail(boolean returnValueOnQueryFail) {
        this.returnValueOnQueryFail = returnValueOnQueryFail;
    }

    @AppInjectionPoint
    final public void setMessageSender(AepMessageSender sender) {
        this.msgSender = sender;
    }

    @EventHandler
    final public void onRqstEmployeeAdd(RqstEmployeeAdd msg, Repository repo) {
        //
        // i will always reply
        //
        RplyEmployeeAdd rply = RplyEmployeeAdd.create();
        rply.setMsgId(msg.getMsgId());

        //
        // find the store
        //
        RetailStore store = failIfAbsentRetailStore(msg, repo, rply);
        if (store == null)
            return;

        //
        // find the employee
        //
        if (failIfPresentEmployee(msg, rply, store))
            return;

        //
        // seems like I don't have the employee either, create, add to map and send success
        //
        succeedAndFinish(msg, rply, store);
    }

    @EventHandler
    final public void onRqstEmployeeQuery(RqstEmployeeQuery msg, Repository repo) {
        //
        // I will always reply
        //
        RplyEmployeeQuery rply = RplyEmployeeQuery.create();
        rply.setMsgId(msg.getMsgId());

        //
        // check the store
        //
        RetailStore rs = repo.getRetailStores().get(msg.getRetailStoreID());
        if (rs == null) {
            failAndFinish(msg, rply, msg.getMsgId(), "Store", msg.getRetailStoreID());
            return;
        }

        //
        // check the employee
        //
        Employee empl = rs.getEmployees().get(msg.getEmployeeID());
        if (empl == null) {
            failAndFinish(msg, rply, msg.getMsgId(), "Employee", msg.getEmployeeID());
            return;
        }

        //
        // yep, got 'em
        //
        EmployeeDTO dst = EmployeeDTO.create();
        empl.getEmployeeDTO().copyInto(dst);
        rply.setEmployee(dst);
        rply.setFound(true);
        msgSender.sendMessage(this.rplyChannel, rply);
        FRLogger.logfmt(Level.FINE, msg.getMsgId(), msg.getMessageChannel(), "Lookup succeded. employeeid:{0}", empl.getEmployeeID());
    }

    private void failAndFinish(RqstEmployeeQuery rqst, RplyEmployeeQuery rply, long msgid, String item, String id) {
        EmployeeDTO dst = EmployeeDTO.create();
        dst.setEmployeeID(rqst.getEmployeeID());
        dst.setEmployeeStoreAssignment(rqst.getRetailStoreID());
        rply.setFound(getReturnValueOnQueryFail());
        rply.setEmployee(dst);
        msgSender.sendMessage(rplyChannel, rply);
        FRLogger.logfmt(Level.CONFIG, msgid, rqst.getMessageChannel(), "employee-query failed. Failed lookup: {0}, ID:{1}", item, id);
    }

    private void succeedAndFinish(RqstEmployeeAdd msg, RplyEmployeeAdd rply, RetailStore store) {
        Employee employee = Employee.create();
        EmployeeDTO dst = EmployeeDTO.create();
        msg.getEmployee().copyInto(dst);
        employee.setEmployeeID(dst.getEmployeeID());
        employee.setEmployeeDTO(dst);
        rply.setEmployeeID(dst.getEmployeeID());
        rply.setSucceeded(true);
        store.getEmployees().put(dst.getEmployeeID(), employee);
        msgSender.sendMessage(rplyChannel, rply);
        FRLogger.logfmt(Level.DIAGNOSE, msg.getMsgId(), msg.getMessageChannel(), "Added new employee:{0}, store:{1}",
                          dst.getEmployeeID(),
                          dst.getEmployeeStoreAssignment());
    }

    private boolean failIfPresentEmployee(RqstEmployeeAdd msg, RplyEmployeeAdd rply, RetailStore store) {
        Employee employee = store.getEmployees().get(msg.getEmployee().getEmployeeID());
        if (employee != null) {
            rply.setSucceeded(false);
            rply.setEmployeeID(msg.getEmployee().getEmployeeID());
            rply.setErrorString("duplciate entry");
            msgSender.sendMessage(rplyChannel, rply);
            FRLogger.logfmt(Level.WARNING, msg.getMsgId(), msg.getMessageChannel(),
                              "Employee already present: {0}", employee.getEmployeeDTO().getEmployeeID());
            return true;
        }
        return false;
    }

    private RetailStore failIfAbsentRetailStore(RqstEmployeeAdd msg, Repository repo, RplyEmployeeAdd rply) {
        RetailStore store = repo.getRetailStores().get(msg.getEmployee().getEmployeeStoreAssignment());
        if (store == null) {
            rply.setSucceeded(false);
            rply.setMsgId(msg.getMsgId());
            rply.setEmployeeID(msg.getEmployee().getEmployeeID());
            rply.setErrorString("store not found");
            msgSender.sendMessage(rplyChannel, rply);
            FRLogger.logfmt(Level.WARNING, msg.getMsgId(), msg.getMessageChannel(),
                              "unable to add employee; store not found:{0}", msg.getRetailStoreID());
            return null;
        }
        return store;
    }
}
