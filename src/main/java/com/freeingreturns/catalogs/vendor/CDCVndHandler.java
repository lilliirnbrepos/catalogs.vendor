/****************************************************************************
 * FILE: CDCHandler.java
 * DSCRPT:
 ****************************************************************************/

package com.freeingreturns.catalogs.vendor;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

import com.eaio.uuid.UUID;
import com.freeingreturns.catalogs.vendor.state.Employee;
import com.freeingreturns.catalogs.vendor.state.RetailStore;
import com.freeingreturns.catalogs.vendor.state.Workstation;
import com.freeingreturns.roe.vendor.EmployeeDTO;
import com.freeingreturns.roe.vendor.RetailStoreDTO;
import com.freeingreturns.roe.vendor.WorkstationDTO;
import com.freeingreturns.utils.FRLogger;
import com.neeve.rog.IRogChangeDataCaptureHandler;
import com.neeve.rog.IRogNode;
import com.neeve.trace.Tracer.Level;

import freeingreturns.catalogs.db.LocalIMDB;
import freeingreturns.catalogs.orm.DbEmployee;
import freeingreturns.catalogs.orm.DbRetailStore;
import freeingreturns.catalogs.orm.DbWorkstation;

public class CDCVndHandler implements IRogChangeDataCaptureHandler {

    private LocalIMDB localDb;





    public CDCVndHandler() {
        FRLogger.logAsIs(Level.SEVERE, "CDCCustHandler::CDCCustHandler()");
        localDb = LocalIMDB.getInstance();
        localDb.connectToDb();
    }





    @Override
    public boolean handleChange(UUID objectId, ChangeType changeType, List<IRogNode> entries) {

        if (changeType == ChangeType.Send || changeType == ChangeType.Noop) {
            return true;
        }

        DbRetailStore dbrs;
        DbEmployee dbem;
        DbWorkstation dbws;
        EmployeeDTO emdto;
        RetailStoreDTO rsdto;
        WorkstationDTO wsdto;
        boolean success = false;
        for (IRogNode node : entries) {

            success = false;

            if (node instanceof RetailStore) {
                rsdto = ((RetailStore)node).getRetailStoreDTO();
                dbrs = new DbRetailStore();
                dbrs.getRetailStoreID().setValue(rsdto.getRetailStoreID());
                dbrs.getRetailStoreGroupId().setValue(rsdto.getRetailStoreGroupId());
                dbrs.getOpenDate().setValue(rsdto.getOpenDate());
                dbrs.getDistrictID().setValue(rsdto.getDistrictID());
                dbrs.getRegionID().setValue(rsdto.getRegionID());
                dbrs.getGeoCode().setValue(rsdto.getGeoCode());
                dbrs.getRecordCreatedTimestamp().setValue(Timestamp.valueOf(LocalDateTime.now()));
                dbrs.getRecordModifiedTimestamp().setValue(Timestamp.valueOf(LocalDateTime.now()));

                if (changeType.equals(ChangeType.Put))
                    success = localDb.addRetailStore(dbrs);
                if (changeType.equals(ChangeType.Update))
                    success = localDb.updateRetailStore(dbrs);

                FRLogger.logfmt(Level.INFO, "[dbworker] - success:{0}, table:{1}, id:{2}",
                                success,
                                dbrs.getTableName(),
                                dbrs.getRetailStoreID().getValue());

            }
            else if (node instanceof Employee) {
                emdto = ((Employee)node).getEmployeeDTO();
                dbem = new DbEmployee();
                dbem.getEmployeeID().setValue(emdto.getEmployeeID());
                dbem.getEmployeeName().setValue(emdto.getEmployeeName());
                dbem.getEmployeeLastName().setValue(emdto.getEmployeeLastName());
                dbem.getEmployeeFirstName().setValue(emdto.getEmployeeFirstName());
                dbem.getEmployeeMiddleName().setValue(emdto.getEmployeeMiddleName());
                dbem.getEmployeeStoreAssignment().setValue(emdto.getEmployeeStoreAssignment());

                if (changeType.equals(ChangeType.Put))
                    success = localDb.addEmployee(dbem);
                if (changeType.equals(ChangeType.Update))
                    success = localDb.updateEmployee(dbem);

                FRLogger.logfmt(Level.INFO, "[dbworker] - success:{0}, table:{1}, id:{2}",
                                success,
                                dbem.getTableName(),
                                dbem.getEmployeeID().getValue());

            }
            else if (node instanceof Workstation) {
                dbws = new DbWorkstation();
                wsdto = ((Workstation)node).getWorkstationDTO();
                dbws.getWorkstationID().setValue(wsdto.getWorkstationID());
                dbws.getRetailStoreID().setValue(wsdto.getRetailStoreID());
                dbws.getManufacturerName().setValue(wsdto.getManufacturerName());
                dbws.getTillCount().setValue(BigDecimal.valueOf(wsdto.getTillCount()));
                dbws.getCurrentTillID().setValue(wsdto.getCurrentTillID());
                dbws.getTillFloatAmount().setValue(BigDecimal.valueOf(wsdto.getTillFloatAmount()));
                dbws.getTransactionSequenceNumber().setValue(wsdto.getTransactionSequenceNumber());

                if (changeType.equals(ChangeType.Put))
                    success = localDb.addWorkstation(dbws);
                else if (changeType.equals(ChangeType.Update))
                    success = localDb.updateWorkstation(dbws);

                FRLogger.logfmt(Level.INFO, "[dbworker] - success:{0}, table:{1}, id:{2}",
                                success,
                                dbws.getTableName(),
                                dbws.getWorkstationID().getValue());

            }

        }
        return true;
    }





    @Override
    public boolean onCheckpointComplete(long checkpointVersion) {
        //logger.log(format("onCheckpointComplete(checkpointVersion={0})", checkpointVersion), Level.INFO);
        return true;
    }





    @Override
    public void onCheckpointStart(long checkpointVersion) {
        //logger.log(format("onCheckpointStart(checkpointVersion={0})", checkpointVersion), Level.INFO);
    }





    @Override
    public void onLogComplete(int logNumber, LogCompletionReason reason, Throwable errorCause) {
        //logger.log(format("onLogComplete(logNumber={0})", logNumber), Level.INFO);
    }





    @Override
    public void onLogStart(int logNumber) {
        //logger.log(format("onLogStart(logNumber={0})", logNumber), Level.INFO);
    }





    @Override
    public void onWait() {}

}
