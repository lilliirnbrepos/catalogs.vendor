package com.freeingreturns.catalogs.vendor;

import java.util.Set;

import com.freeingreturns.catalogs.vendor.state.Repository;
import com.freeingreturns.catalogs.vendor.state.RetailStore;
import com.freeingreturns.roe.vendor.RplyVendorQuery;
import com.freeingreturns.roe.vendor.RqstVendorQuery;
import com.freeingreturns.utils.FRLogger;
import com.freeingreturns.utils.FRUnhandleExcpetionCatcher;
import com.neeve.aep.AepEngine;
import com.neeve.aep.AepMessageSender;
import com.neeve.aep.IAepApplicationStateFactory;
import com.neeve.aep.annotations.EventHandler;
import com.neeve.aep.event.AepEngineStoppingEvent;
import com.neeve.aep.event.AepMessagingStartedEvent;
import com.neeve.cli.annotations.Command;
import com.neeve.rog.log.RogLog;
import com.neeve.server.app.annotations.AppConfiguredAccessor;
import com.neeve.server.app.annotations.AppEventHandlerContainersAccessor;
import com.neeve.server.app.annotations.AppHAPolicy;
import com.neeve.server.app.annotations.AppInjectionPoint;
import com.neeve.server.app.annotations.AppStateFactoryAccessor;
import com.neeve.sma.MessageView;
import com.neeve.trace.Tracer.Level;

@AppHAPolicy(value = AepEngine.HAPolicy.StateReplication)
public class VendorCatalogMain extends VndProps {

    protected EmployeeCatalog emCatalog;
    protected RetailStoreCatalog rsCatalog;
    protected WorkstationCatalog wsCatalog;





    public VendorCatalogMain() {
        emCatalog = new EmployeeCatalog();
        rsCatalog = new RetailStoreCatalog();
        wsCatalog = new WorkstationCatalog();
        Thread.setDefaultUncaughtExceptionHandler(FRUnhandleExcpetionCatcher.getInstance());
    }





    @AppStateFactoryAccessor
    final public IAepApplicationStateFactory getStateFactory() {
        return new IAepApplicationStateFactory() {
            @Override
            final public Repository createState(MessageView view) {

                return Repository.create();
            }
        };
    }





    @AppInjectionPoint
    public void injectAepEngine(AepEngine engine) {
        aepEngine = engine;
    }





    @EventHandler
    final public void onMessagingStarted(final AepMessagingStartedEvent event) {
        Thread t = new Thread(new Runnable() {
            public void run() {
                FRLogger.logAsIs(Level.INFO, "Engine activated, starting CDC");
                try {
                    startCdc();
                }
                catch (Exception e_) {
                    e_.printStackTrace();
                }
            }
        });
        t.start();
    }





    @EventHandler
    public void onEngineStopped(AepEngineStoppingEvent event) {
        FRLogger.logAsIs(Level.INFO, "Engine stopping, stopping CDC");
        try {
            stopCdc();
        }
        catch (Exception e_) {
            e_.printStackTrace();
        }
    }





    @Command(name = "Start CDC")
    public void startCdc() throws Exception {
        Boolean enabled = false;
        try {
            enabled = Boolean.valueOf(aepEngine.getStore().getPersister().getDescriptor().getProperty(RogLog.PROP_CDC_ENABLED));
            if (enabled) {
                log = (RogLog)aepEngine.getStore().getPersister();
                cdchandler = new CDCVndHandler();
                processor = log.createCdcProcessor(cdchandler);
                processor.run();
                return;
            }
        }
        catch (Exception e) {}
        FRLogger.logAsIs(Level.WARNING, "CDC Storage disabled");
    }





    @Command(name = "Stop CDC")
    public void stopCdc() throws Exception {
        if (processor != null)
            processor.close();
    }





    @AppEventHandlerContainersAccessor
    public void getEventHandlers(Set<Object> handlers) {
        handlers.add(emCatalog);
        handlers.add(rsCatalog);
        handlers.add(wsCatalog);
        FRLogger.logAsIs(Level.CONFIG, "Finished creating event handlers");
    }





    @AppConfiguredAccessor
    public void addConfiguredObjects(Set<Object> cfgs) {
        cfgs.add(emCatalog);
        cfgs.add(rsCatalog);
        cfgs.add(wsCatalog);
    }





    @AppInjectionPoint
    final public void setMessageSender(AepMessageSender sender) {
        msgSender = sender;
        emCatalog.setMessageSender(msgSender);
        rsCatalog.setMessageSender(msgSender);
        wsCatalog.setMessageSender(msgSender);
        FRLogger.logAsIs(Level.CONFIG, "Finished message sender initialization");
    }





    @EventHandler
    final public void onRqstVendorQuery(RqstVendorQuery msg, Repository repo) {
        RplyVendorQuery rply = RplyVendorQuery.create();
        rply.setMsgId(msg.getMsgId());
        rply.setRetailStoreID(msg.getRetailStoreID());

        RetailStore rs = repo.getRetailStores().get(msg.getRetailStoreID());
        if (rs == null) {
            FRLogger.logfmt(Level.WARNING, msg.getMsgId(), msg.getMessageChannel(), "Unable to find vendor:{0}", msg.getRetailStoreID());
            rply.setFound(false);
            msgSender.sendMessage(rplyChannel, rply);
            return;
        }

        FRLogger.logfmt(Level.WARNING, msg.getMsgId(), msg.getMessageChannel(), "FOUND vendor:{0}", msg.getRetailStoreID());
        Set<String> wsSet = rs.getWorkstations().keySet();
        Set<String> emSet = rs.getEmployees().keySet();

        for (String id : wsSet)
            rply.addWorkstationID(id);

        for (String id : emSet) {
            if (id != null)
                rply.addEmployeeID(id);
        }

        rply.setFound(true);
        msgSender.sendMessage(rplyChannel, rply);
        return;
    }

}
