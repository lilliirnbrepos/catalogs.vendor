/****************************************************************************
 * FILE: RetailStoreCatalog.java
 * DSCRPT:
 ****************************************************************************/

package com.freeingreturns.catalogs.vendor;

import com.freeingreturns.catalogs.vendor.state.Repository;
import com.freeingreturns.catalogs.vendor.state.RetailStore;
import com.freeingreturns.roe.vendor.RetailStoreDTO;
import com.freeingreturns.roe.vendor.RplyRetailStoreAdd;
import com.freeingreturns.roe.vendor.RplyRetailStoreQuery;
import com.freeingreturns.roe.vendor.RqstRetailStoreAdd;
import com.freeingreturns.roe.vendor.RqstRetailStoreQuery;
import com.freeingreturns.utils.FRLogger;
import com.neeve.aep.AepMessageSender;
import com.neeve.aep.annotations.EventHandler;
import com.neeve.cli.annotations.Configured;
import com.neeve.server.app.annotations.AppInjectionPoint;
import com.neeve.trace.Tracer;
import com.neeve.trace.Tracer.Level;

public class RetailStoreCatalog extends VndProps {

    @AppInjectionPoint
    final public void setMessageSender(AepMessageSender sender) {
        this.msgSender = sender;
    }





    @EventHandler
    final public void onRqstRetailStoreAdd(RqstRetailStoreAdd msg, Repository repo) {
        //
        // i will always reply
        //
        RplyRetailStoreAdd rply = RplyRetailStoreAdd.create();
        rply.setMsgId(msg.getMsgId());

        //
        // do I have the store?
        //
        if (failIfPresentRetailStore(msg, repo, rply))
            return;

        //
        // looks like I am fine, 
        // copy data, add into map & send success
        //
        succeedAndFinish(msg, repo, rply);
    }





    @EventHandler
    final public void onRqstRetailStoreQuery(RqstRetailStoreQuery msg, Repository repo) {
        //
        // i will always reply
        //
        RplyRetailStoreQuery rply = RplyRetailStoreQuery.create();
        rply.setMsgId(msg.getMsgId());

        //
        // find the store
        //
        RetailStore store = repo.getRetailStores().get(msg.getRetailStoreID());
        if (store == null) {
            rply.setFound(returnValueOnQueryFail);
            RetailStoreDTO dto = RetailStoreDTO.create();
            dto.setRetailStoreID(msg.getRetailStoreID());
            rply.setRetailStore(dto);
            msgSender.sendMessage(rplyChannel, rply);
            FRLogger.logfmt(Level.WARNING, msg.getMsgId(), msg.getMessageChannel(), "Unable to lookup store, id: {0}", msg.getRetailStoreID());
            return;
        }

        //
        // copy in requisite info, set repose to true & send
        //
        succeedAndFinish(msg, rply, store);
    }





    private void succeedAndFinish(RqstRetailStoreAdd msg, Repository repo, RplyRetailStoreAdd rply) {
        String storeId = msg.getRetailStore().getRetailStoreID();
        RetailStore rs = RetailStore.create();
        RetailStoreDTO dto = RetailStoreDTO.create();
        msg.getRetailStore().copyInto(dto);
        rs.setRetailStoreDTO(dto);
        rs.setRetailStoreID(storeId);
        dto.setRetailStoreID(storeId);
        repo.getRetailStores().put(storeId, rs);
        rply.setRetailStoreID(storeId);
        rply.setSucceeded(true);
        msgSender.sendMessage(rplyChannel, rply);
        FRLogger.logfmt(Level.INFO, msg.getMsgId(), msg.getMessageChannel(), "Added new reatil-store. id: {0}", rs.getRetailStoreDTO().getRetailStoreID());
    }





    private boolean failIfPresentRetailStore(RqstRetailStoreAdd msg, Repository repo, RplyRetailStoreAdd rply) {
        RetailStore rs = repo.getRetailStores().get(msg.getRetailStore().getRetailStoreID());
        if (rs != null) {
            rply.setSucceeded(false);
            rply.setRetailStoreID(msg.getRetailStore().getRetailStoreID());
            rply.setErrorString("duplicate entry");
            msgSender.sendMessage(rplyChannel, rply);
            FRLogger.logfmt(Level.WARNING, msg.getMsgId(), msg.getMessageChannel(), "RS already present, rsid: {0}", rs.getRetailStoreID());
            return true;
        }
        return false;
    }





    private void succeedAndFinish(RqstRetailStoreQuery msg, RplyRetailStoreQuery rply, RetailStore store) {
        String storeId = msg.getRetailStoreID();
        RetailStoreDTO dst = RetailStoreDTO.create();
        store.getRetailStoreDTO().copyInto(dst);

        // due to easing restrictions, a lot more things are allowed to be null now... manually set it
        if (store.getRetailStoreDTO().getRetailStoreID() == null) {
            store.getRetailStoreDTO().setRetailStoreID(storeId);
            dst.setRetailStoreID(storeId);
        }

        rply.setRetailStore(dst);
        rply.setFound(true);
        msgSender.sendMessage(rplyChannel, rply);
        FRLogger.logfmt(Level.DIAGNOSE, msg.getMsgId(), msg.getMessageChannel(), "found rsid: {0}", msg.getRetailStoreID());
    }
}
