package com.freeingreturns.catalogs.vendor.driver;

import static java.text.MessageFormat.format;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

import com.freeingreturns.roe.vendor.EmployeeDTO;
import com.freeingreturns.roe.vendor.RetailStoreDTO;
import com.freeingreturns.roe.vendor.RplyEmployeeAdd;
import com.freeingreturns.roe.vendor.RplyRetailStoreAdd;
import com.freeingreturns.roe.vendor.RplyWorkstationAdd;
import com.freeingreturns.roe.vendor.RqstEmployeeAdd;
import com.freeingreturns.roe.vendor.RqstRetailStoreAdd;
import com.freeingreturns.roe.vendor.RqstWorkstationAdd;
import com.freeingreturns.roe.vendor.WorkstationDTO;
import com.neeve.aep.AepEngine;
import com.neeve.aep.AepMessageSender;
import com.neeve.aep.annotations.EventHandler;
import com.neeve.cli.annotations.Argument;
import com.neeve.cli.annotations.Command;
import com.neeve.cli.annotations.Configured;
import com.neeve.server.app.annotations.AppInjectionPoint;
import com.neeve.server.app.annotations.AppMain;
import com.neeve.server.app.annotations.AppStat;
import com.neeve.stats.IStats.Counter;
import com.neeve.stats.StatsFactory;
import com.neeve.trace.Tracer;
import com.neeve.trace.Tracer.Level;
import com.neeve.util.UtlGovernor;

import freeingreturns.catalogs.datagen.RetailStoreGenerator;
import freeingreturns.catalogs.orm.DbEmployee;
import freeingreturns.catalogs.orm.DbRetailStore;
import freeingreturns.catalogs.orm.DbWorkstation;

/**
 * A test driver app for the Application.
 */
public class BasicFlow {
    private static final Tracer logger = Tracer.create("service.vendor", Level.DEBUG);

    private final AtomicReference<Thread> sendingThread = new AtomicReference<Thread>();
    private volatile AepMessageSender messageSender;
    private AepEngine engine;

    private CountDownLatch latch;

    @Configured(property = "service.vendor.tests.basic.autoStart", defaultValue="true")
    private boolean autoStart;

    @Configured(property = "service.vendor.tests.basic.sendCount", defaultValue="1")
    private int sendCount;

    @Configured(property = "service.vendor.tests.basic.sendRate", defaultValue="1")
    private int sendRate;

    @Configured(property = "service.vendor.channels.requestChannel", defaultValue="VendorRequestChannel")
    private String rqstChannel;

    @AppStat
    private final Counter sentCount = StatsFactory.createCounterStat("SendDriver Count");

    @AppInjectionPoint
    final public void setMessageSender(AepMessageSender messageSender) {
        this.messageSender = messageSender;
    }

    @AppInjectionPoint
    final public void setAepEngine(AepEngine engine) {
        this.engine = engine;
    }

    /**
     * Starts sending messages (in a background thread)
     */
    @Command(name = "send", displayName = "Send Messages", description = "Instructs the driver to send messages")
    public final void doSend(@Argument(name = "count", position = 1, required = true, description = "The number of messages to send") final int count,
                             @Argument(name = "rate", position = 2, required = true, description = "The rate at which to send") final int rate) throws Exception {
        int latchCount = count * 7;
        logger.log(format("Total number of messages expected to generate:{0}",  latchCount), Level.INFO);
        latch = new CountDownLatch(latchCount);
        engine.waitForMessagingToStart();
        final Thread thread = new Thread(new SenderThread(count, rate), "Basic Flow Driver");
        startSending(thread);
    }

    /**
     * 
     */
    private void startSending(final Thread thread) throws InterruptedException {
        Thread oldThread = sendingThread.getAndSet(thread);
        if (oldThread != null) {
            oldThread.interrupt();
            oldThread.join();
        }
        thread.start();
    }

    /**
     * Stops the current sending thread (if active_ 
     */
    @Command(name = "stopSending", displayName = "Stop Sending", description = "Stops sending of messages.")
    final public void stop() throws Exception {
        Thread oldThread = sendingThread.getAndSet(null);
        if (oldThread != null) {
            oldThread.join();
        }
    }

    /**
     * Gets the number of messages sent by the sender. 
     * 
     * @return The number of messages sent by this sender.
     */
    @Command
    public long getSentCount() {
        return sentCount.getCount();
    }

    @AppMain
    public void run(String[] args) throws Exception {
        if (autoStart) {
            doSend(sendCount, sendRate);
        }
    }

    /**
     * 
     */
    public final CountDownLatch getLatch() {
        return latch;
    }

    private class SenderThread implements Runnable {
        private int count;
        private int rate;

        public SenderThread(int count, int rate) {
            this.count = count;
            this.rate = rate;
        }

        @Override
        public void run() {
            RetailStoreGenerator generator = new RetailStoreGenerator();
            List<DbRetailStore> list = generator.generate(count);
            Iterator<DbRetailStore> itr = list.iterator();

            final UtlGovernor sendGoverner = new UtlGovernor(rate);
            RqstRetailStoreAdd rsAdd;
            RqstWorkstationAdd wsAdd;
            RqstEmployeeAdd emAdd;

            logger.log(format("About to send: {0}", list.size()), Level.INFO);
            DbRetailStore store;
            DbWorkstation station;
            DbEmployee empl;
            WorkstationDTO ws;
            EmployeeDTO em;
            Iterator<DbWorkstation> wItr;
            Iterator<DbEmployee> eItr;
            String retailStoreId;
            int msgid = 1;
            while (itr.hasNext()) {
                if (sendingThread.get() != Thread.currentThread())
                    break; // someone swapped me out

                store = itr.next();
                rsAdd = RqstRetailStoreAdd.create();
                rsAdd.setRetailStore(RetailStoreDTO.create());
                rsAdd.getRetailStore().setRetailStoreID(store.getRetailStoreID().getValue());
                rsAdd.getRetailStore().setRetailStoreGroupId(store.getRetailStoreGroupId().getValue());
                rsAdd.getRetailStore().setOpenDate(store.getOpenDate().getValue());
                rsAdd.getRetailStore().setDistrictID(store.getDistrictID().getValue());
                rsAdd.getRetailStore().setGeoCode(store.getGeoCode().getValue());
                retailStoreId = store.getRetailStoreID().getValue();
                logger.log("Just sent retail store id:" + retailStoreId, Level.WARNING);
                rsAdd.setMsgId(msgid++);
                messageSender.sendMessage(rqstChannel, rsAdd);
                sentCount.increment();

                wItr = store.getWorkstations().iterator();
                while (wItr.hasNext()) {
                    station = wItr.next();
                    wsAdd = RqstWorkstationAdd.create();
                    wsAdd.setWorkstation(WorkstationDTO.create());
                    ws = wsAdd.getWorkstation();
                    ws.setWorkstationID(station.getWorkstationID().getValue());
                    ws.setRetailStoreID(station.getRetailStoreID().getValue());
                    ws.setCurrentTillID(station.getCurrentTillID().getValue());
                    ws.setTillCount(station.getTillCount().getValue().intValue());
                    ws.setTillFloatAmount(station.getTillFloatAmount().getValue().doubleValue());
                    ws.setWorkstationID(station.getWorkstationID().getValue());
                    wsAdd.setMsgId(msgid++);
                    logger.log("Just sent retail-store id:" + retailStoreId + ", wsid:" + ws.getWorkstationID(), Level.WARNING);
                    messageSender.sendMessage(rqstChannel, wsAdd);
                }

                eItr = store.getEmployees().iterator();
                while (eItr.hasNext()) {
                    empl = eItr.next();
                    emAdd = RqstEmployeeAdd.create();
                    em = EmployeeDTO.create();
                    emAdd.setEmployee(em);
                    em.setEmployeeID(empl.getEmployeeID().getValue());
                    em.setEmployeeStoreAssignment(empl.getEmployeeStoreAssignment().getValue());
                    emAdd.setMsgId(msgid++);
                    emAdd.setRetailStoreID(retailStoreId);
                    logger.log("Just sent retail-store id:" + retailStoreId + ", emid:" + empl.getEmployeeID().getValue(), Level.WARNING);
                    messageSender.sendMessage(rqstChannel, emAdd);
                }

                sendGoverner.blockToNext();
            }
        }
    }

    @EventHandler
    public void onEvent(RplyRetailStoreAdd rply) {
        logger.log(rply.toString(), Level.INFO);
        latch.countDown();
    }

    @EventHandler
    public void onEvent(RplyWorkstationAdd rply) {
        logger.log(rply.toString(), Level.INFO);
        latch.countDown();
    }

    @EventHandler
    public void onEvent(RplyEmployeeAdd rply) {
        logger.log(rply.toString(), Level.INFO);
        latch.countDown();
    }

}
