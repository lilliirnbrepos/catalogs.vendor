/****************************************************************************
 * FILE: WorkstationCatalog.java
 * DSCRPT:
 ****************************************************************************/

package com.freeingreturns.catalogs.vendor;

import com.freeingreturns.catalogs.vendor.state.Repository;
import com.freeingreturns.catalogs.vendor.state.RetailStore;
import com.freeingreturns.catalogs.vendor.state.Workstation;
import com.freeingreturns.roe.vendor.RplyWorkstationAdd;
import com.freeingreturns.roe.vendor.RplyWorkstationQuery;
import com.freeingreturns.roe.vendor.RqstWorkstationAdd;
import com.freeingreturns.roe.vendor.RqstWorkstationQuery;
import com.freeingreturns.roe.vendor.WorkstationDTO;
import com.freeingreturns.utils.FRLogger;
import com.neeve.aep.AepMessageSender;
import com.neeve.aep.annotations.EventHandler;
import com.neeve.cli.annotations.Configured;
import com.neeve.server.app.annotations.AppInjectionPoint;
import com.neeve.trace.Tracer.Level;

public class WorkstationCatalog extends VndProps {

    @AppInjectionPoint
    final public void setMessageSender(AepMessageSender sender) {
        this.msgSender = sender;
    }





    @EventHandler
    final public void onRqstWorkstationAdd(RqstWorkstationAdd msg, Repository repo) {
        //
        // i will always reply
        //
        RplyWorkstationAdd rply = RplyWorkstationAdd.create();
        rply.setMsgId(msg.getMsgId());

        //
        // find the store
        //
        RetailStore store = failIfAbsentRetailStore(msg, repo, rply);
        if (store == null)
            return;

        //
        // find the workstation
        //
        if (failIfPresentWorkstation(msg, rply, store))
            return;

        //
        // looks like I am good, 
        // create the workstation, add to maps, and send succsess
        //
        succeedAndFinish(msg, rply, store);
    }





    @EventHandler
    final public void onRqstWorkstationQuery(RqstWorkstationQuery msg, Repository repo) {
        //
        // i will always reply
        //
        RplyWorkstationQuery rply = RplyWorkstationQuery.create();
        rply.setMsgId(msg.getMsgId());

        //
        // find the store
        //
        RetailStore store = failIfAbsentRetailStore(msg, repo, rply);
        if (store == null)
            return;

        //
        // find the workstation
        //
        Workstation workstation = failIfAbsentWorkstation(msg, rply, store);
        if (workstation == null)
            return;

        //
        // looks like I am good, 
        // create the workstation, add to maps, and send succsess
        //
        succeedAndFinish(msg, rply, workstation);
    }





    private void succeedAndFinish(RqstWorkstationAdd msg, RplyWorkstationAdd rply, RetailStore store) {
        Workstation ws = Workstation.create();
        WorkstationDTO dst = WorkstationDTO.create();
        msg.getWorkstation().copyInto(dst);
        ws.setWorkstationID(dst.getWorkstationID());
        ws.setWorkstationDTO(dst);
        rply.setSucceeded(true);
        rply.setWorkstationID(ws.getWorkstationID());
        store.getWorkstations().put(ws.getWorkstationID(), ws);
        msgSender.sendMessage(rplyChannel, rply);
        FRLogger.logfmt(Level.DIAGNOSE, msg.getMsgId(), msg.getMessageChannel(), "new workstation added, store: {0}, workstation: {1}", store.getRetailStoreID(), ws.getWorkstationID());
    }





    private void succeedAndFinish(RqstWorkstationQuery msg, RplyWorkstationQuery rply, Workstation ws) {
        WorkstationDTO dst = WorkstationDTO.create();
        ws.getWorkstationDTO().copyInto(dst);
        rply.setFound(true);
        rply.setWorkstation(dst);
        msgSender.sendMessage(rplyChannel, rply);
        FRLogger.logfmt(Level.DIAGNOSE, msg.getMsgId(), msg.getMessageChannel(), "found workstion: {0}, retail-store: {1}", dst.getWorkstationID(), msg.getRetailStoreID());
    }





    private boolean failIfPresentWorkstation(RqstWorkstationAdd msg, RplyWorkstationAdd rply, RetailStore store) {
        Workstation ws = store.getWorkstations().get(msg.getWorkstation().getWorkstationID());
        if (ws != null) {
            WorkstationDTO dst = WorkstationDTO.create();
            msg.getWorkstation().copyInto(dst);
            rply.setWorkstationID(dst.getWorkstationID());
            rply.setSucceeded(false);
            rply.setErrorString("duplicate entry");
            msgSender.sendMessage(rplyChannel, rply);
            FRLogger.logfmt(Level.WARNING, msg.getMsgId(), msg.getMessageChannel(),
                            "Unable to add workstation to retail store - Error: {1}. WSID: {0}",
                            dst.getWorkstationID(), "duplciate entry");
            return true;
        }
        return false;
    }





    private Workstation failIfAbsentWorkstation(RqstWorkstationQuery msg, RplyWorkstationQuery rply, RetailStore store) {
        Workstation ws = store.getWorkstations().get(msg.getWorkstationID());
        if (ws == null) {
            FRLogger.logfmt(Level.WARNING, msg.getMsgId(), msg.getMessageChannel(),
                            "unable to find workstation/store - workstation not present. WSID: {0}",
                            msg.getWorkstationID());
            WorkstationDTO dst = WorkstationDTO.create();
            dst.setWorkstationID(msg.getWorkstationID());
            rply.setFound(getReturnValueOnQueryFail());
            rply.setWorkstation(dst);
            msgSender.sendMessage(rplyChannel, rply);
            return null;
        }
        return ws;
    }





    private RetailStore failIfAbsentRetailStore(RqstWorkstationAdd msg, Repository repo, RplyWorkstationAdd rply) {
        String storeID = msg.getWorkstation().getRetailStoreID();
        RetailStore store = repo.getRetailStores().get(storeID);
        if (store == null) {
            rply.setSucceeded(getReturnValueOnQueryFail());
            rply.setWorkstationID(msg.getWorkstation().getWorkstationID());
            rply.setErrorString("store not present");
            msgSender.sendMessage(rplyChannel, rply);
            FRLogger.logfmt(Level.WARNING,
                            msg.getMsgId(),
                            msg.getMessageChannel(),
                            "unable to add worstation/store, store not present. store-id: {0}", msg.getWorkstation().getRetailStoreID());
            return null;
        }

        return store;
    }





    private RetailStore failIfAbsentRetailStore(RqstWorkstationQuery msg, Repository repo, RplyWorkstationQuery rply) {
        String storeID = msg.getRetailStoreID();
        RetailStore store = repo.getRetailStores().get(storeID);
        if (store == null) {
            WorkstationDTO dst = WorkstationDTO.create();
            dst.setWorkstationID(msg.getWorkstationID());
            rply.setFound(getReturnValueOnQueryFail());
            rply.setWorkstation(dst);
            msgSender.sendMessage(rplyChannel, rply);
            FRLogger.logfmt(Level.WARNING,
                            msg.getMsgId(),
                            msg.getMessageChannel(),
                            "unable to add worstation/store, store not present. store-id: {0}", msg.getRetailStoreID());
            return null;
        }

        return store;
    }

}
